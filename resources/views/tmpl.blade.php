<!DOCTYPE html>
<html lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="@lang('app.description')">
        <meta name="author" content="@yield('author')">
        <link rel="icon" href="{{ asset('favico.ico') }}">

        <title>{{ Config::get('tmpl.title') }} - @yield('page.title')</title>

        <!-- Compiled app css --> 
        <link href="{{ asset('css/vendor.css') }}" rel="stylesheet">
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">

        <link rel="stylesheet" href="https://i.icomoon.io/public/temp/f7c6e38a80/taskadabra/style.css">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">

            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="#">
                        <span class="{{ Config::get('tmpl.logo-class') }}"></span>
                        {{ Config::get('tmpl.title') }}
                    </a>
                </div>

                <!-- language button -->
                <form class="navbar-form navbar-right" method="POST" id="navbar-form">
                    <span>&nbsp;</span>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <!-- login status -->
                    @if (Auth::check())
                    <p class="navbar-text ">@lang('tmpl.logged-as') <a href="#" class="navbar-link">{{ Auth::user() }}</a></p>
                    @else
                    <button type="button" class="btn btn-primary navbar-btn ">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> @lang('tmpl.signup')
                    </button>
                    <button type="button" class="btn btn-default navbar-btn">
                        <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> @lang('tmpl.login')
                    </button>
                    @endif

                    
                    @include('language')
                    

                    <span>&nbsp;</span>
                </form>


            </div>
        </div

    </nav>




    @yield('content')

    <!-- Scripts -->
    <script src="{{ asset('js/vendor.js') }}"></script>

</body>
</html>
