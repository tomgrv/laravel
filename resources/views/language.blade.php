
<div class="form-horizontal btn-group" id="lang-detector">
    <button type="button" class="btn btn-default dropdown-toggle  navbar-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="lang-sm" lang="{{  Lang::getLocale() }}"></span>&nbsp;<span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
        @foreach (Cache::get('app.available-lang') as $language)
        <li><a href=".?lang={{ $language}}"><span class="lang-sm lang-lbl-full" lang="{{ $language}}"></span></a></li>
        @endforeach
    </ul>
</div>