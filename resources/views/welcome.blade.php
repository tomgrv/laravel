@extends('tmpl')

@section('content')
<!-- Full Width Image Header with Logo -->
<!-- Image backgrounds are set within the full-width-pics.css file. -->
<header class="image-bg-fluid-height">
    <a href="./"><span class="taskadabra-logo-round fa-adjust" style="font-size: 10em; color: white"></span></a>
</header>
@endsection
