<?php

namespace App\Http\Middleware;

use Closure;
use Cache;

class DetectLocale
{
    /**
     * Available Languages.
     *
     * @var array
     */
    private $languages;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get Language
        $language = $this->getLanguage($request);

        // Check if supported
        $this->languages = $this->getSupportedLocales();
        $locale = $this->getAliasedLocale($language, config('app.fallback_locale'));

        // Store
        $request->session()->put('app.language_locale', $locale);
        
        // Set Locale
        app()->setLocale($locale);

        return $next($request);
    }

    /**
     * get locales defined in resources/lang directory
     * 
     * @return array
     */
    private function getLanguage($request)
    {
        if($request->session()->has('app.language_locale'))
        {
            return $request->input('lang', $request->session()->get('app.language_locale'));
        }
        else
        {
            return $request->input('lang', $request->getPreferredLanguage($request->getLanguages()));
        }
    }

    /**
     * get locales defined in resources/lang directory
     * 
     * @return array
     */
    private function getSupportedLocales()
    {
        if(Cache::has('app.available-lang'))
        {
            return Cache::get('app.available-lang');
        }

        $languages = \File::directories(app()->langPath());

        array_walk($languages, function(&$value, $key)
        {
            $value = basename($value);
        });

        Cache::forever('app.available-lang', $languages);

        return $languages;
    }

    /**
     * Return the real locale based on available languages.
     *
     * @param string $locale
     * @return string|null
     */
    public function getAliasedLocale($locale, $default = null)
    {
        if(isset($this->languages[$locale]))
        {
            return $this->languages[$locale];
        }

        return in_array($locale, $this->languages) ? $locale : $default;
    }
}
