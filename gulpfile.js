var elixir = require('laravel-elixir');
require('laravel-elixir-bower');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

var options = {};
options.debugging = true;

elixir(function(mix) {
    mix
        .less('app.less')
        .bower(options);
});
